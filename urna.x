#define PROGRAM_NUMBER 1111110
#define VERSION_NUMBER 1

struct candidato{
	int numero;
	int votos;
	char nome[40];
	char foto[40];
};


program URNA_PROG
{
	version URNA_VERSION
	{
		int VOTAR(int numero) = 1;
		candidato BUSCAR_DADOS(int numero) = 2;
		candidato VERIFICAR_VENCEDOR() = 3;
        int ESTABELECER_COMUNICACAO() = 4;
        int FINALIZAR_COMUNICACAO() = 5;
        int VOTAR_BRANCO() = 6;
	}
	=VERSION_NUMBER;
}
=PROGRAM_NUMBER;
