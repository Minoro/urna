#compilador rpc
rpc = rpcgen

#compilador cc
CC = cc

all: stub server cliente

apagar:
	rm cliente server urna_svc.c urna_clnt.c urna_xdr.c

server: 
	$(CC) servidor_urna.c urna_svc.c urna_xdr.c -o server  -lnsl

cliente:	
	$(CC) cliente_urna.c urna_clnt.c urna_xdr.c -o cliente -lnsl

stub:
	$(rpc) urna.x
