#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "urna.h"

//adicionar nome fotos candidatos
static candidato concorrente[3] = {
	{111, 0, "Patata", "cand1.jpg"},
	{222, 0, "Patati", "cand2.jpg"},
	{333, 0, "Bozo", "cand3.jpg"}
};

static candidato concorrente[3];

static candidato NAO_ENCONTRADO = {
    -2, 0, "NULO", "nao_encontrado.jpg"
};

static candidato VOTO_EM_BRANCO = {
    -1, 0, "BRANCO", "nao_encontrado.jpg"
};

static int numero_urnas = 0;

int *estabelecer_comunicacao_1_svc(void *pvoid, struct svc_req *rqstp){
    numero_urnas++;

    printf("Total de urnas: %d\n", numero_urnas );
    return (&numero_urnas);
}

int *finalizar_comunicacao_1_svc(void *pvoid, struct svc_req *rqstp){
    numero_urnas--;
    return (&numero_urnas);
}

int *votar_branco_1_svc(void *pvoid, struct svc_req *rqstp){
    VOTO_EM_BRANCO.votos++;
    return (&VOTO_EM_BRANCO.votos);
}

int *votar_1_svc(int *numero, struct svc_req *rqstp){
	int i = 0;
	static int ok = 1;
	while(*numero != concorrente[i].numero){
		i++;
        if(i > 2) {
            NAO_ENCONTRADO.votos++;
            ok = 0;
            return &ok;
        };
	}
	concorrente[i].votos += 1; 
	return &ok;
}

candidato *buscar_dados_1_svc(int *num, struct svc_req *rqstp){
	int i = 0;
	int numero = *num;

	static candidato temp;
	while(numero != concorrente[i].numero){
		i++;
		if(i > 2) {
			temp.numero = NAO_ENCONTRADO.numero;
			temp.votos = NAO_ENCONTRADO.votos;
			strcpy(temp.nome, NAO_ENCONTRADO.nome);
			strcpy(temp.foto, NAO_ENCONTRADO.foto);
			return &temp;
		}
	}	
	temp.numero = concorrente[i].numero;
	temp.votos = concorrente[i].votos;
	strcpy(temp.nome, concorrente[i].nome);
	strcpy(temp.foto, concorrente[i].foto);

	return &temp;
}

candidato *verificar_vencedor_1_svc(void *pvoid, struct svc_req *rqstp){
    //voto_em_branco == -1
    //votos_nulos == -2
	int i;
	int vencedor = 0;
	int votos_vencedor;
    
    if(VOTO_EM_BRANCO.votos > NAO_ENCONTRADO.votos) {
        vencedor = -1;
        votos_vencedor = VOTO_EM_BRANCO.votos;
    }
    else {
        vencedor = -2;
        votos_vencedor = NAO_ENCONTRADO.votos;
    }
	for(i = 0; i < 3; i++){
		if(concorrente[i].votos > votos_vencedor){
			votos_vencedor = concorrente[i].votos;
			vencedor = i;
		}
	}
    if(vencedor == -1)
        return &VOTO_EM_BRANCO;
    if(vencedor == -2)
        return &NAO_ENCONTRADO;
	return (&concorrente[vencedor]);
}
