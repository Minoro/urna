#-*- coding:utf-8 -*-

from ctypes import *
from Tkinter import *
import tkMessageBox
import sys, os, commands
from PIL import Image


class Candidato(Structure):
	_fields_ = [("numero", c_int), ("votos", c_int), ("nome", c_char*40)]


class Urna_Gui:

	def __init__(self, toplevel):

		self.cliente = cdll.LoadLibrary("/home/minoro/cliente.so")

		self.cliente.votar.restype = c_int
		self.cliente.votar.argtypes = [c_int]

		self.cliente.votar_branco.restype = c_int
		self.cliente.votar_branco.argtypes = []

		self.cliente.buscar_dados.restype = c_void_p
		self.cliente.buscar_dados.argtypes = [c_int]

		self.cliente.verificar_vencedor.restype = c_void_p
		self.cliente.verificar_vencedor.argtypes = []

		self.cliente.finalizar_comunicacao.restype = c_int
		self.cliente.finalizar_comunicacao.argtypes = []

		self.cliente.conectar(sys.argv[1])

		toplevel.title('Urna')
		toplevel.geometry('500x350')

		self.frame1 = Frame(toplevel)
		self.frame1.pack()

		#cria os frames para os botoes
		self.frame_numeros = Frame(toplevel)
		self.frame_numeros.pack()

		self.frame_numeros_top = Frame(self.frame_numeros)
		self.frame_numeros_top.pack()

		self.frame_numeros_linhas3 = Frame(self.frame_numeros)
		self.frame_numeros_linhas3.pack()
		
		self.frame_numeros_linhas4 = Frame(self.frame_numeros)
		self.frame_numeros_linhas4.pack()


		#display dos numeros
		self.display = Text(self.frame_numeros_top, bd=2, height='1', width='10', font='Arial')

		# Label(self.frame_numeros, text='ADJIOASJ', fg='black', font=('Verdana', 10), height= 1).pack()
		# fonte1=('Verdana', 10)

		#define os botões
		self.num_1 = Button(self.frame_numeros_linhas4, text='1',
						command = lambda:self.pressionar(1))

		self.num_1.pack(side=LEFT)
		self.num_2 = Button(self.frame_numeros_linhas4, text='2',
						command = lambda:self.pressionar(2))
		self.num_2.pack(side=LEFT)
		self.num_3 = Button(self.frame_numeros_linhas4, text='3',
						command = lambda:self.pressionar(3))
		self.num_3.pack(side=LEFT)
		self.num_4 = Button(self.frame_numeros_linhas3, text='4',
						command = lambda:self.pressionar(4))
		self.num_4.pack(side=LEFT)
		self.num_5 = Button(self.frame_numeros_linhas3, text='5',
						command = lambda:self.pressionar(5))
		self.num_5.pack(side=LEFT)
		self.num_6 = Button(self.frame_numeros_linhas3, text='6',
						command = lambda:self.pressionar(6))
		self.num_6.pack(side=LEFT)

		self.num_7 = Button(self.frame_numeros_top, text='7',
						command = lambda:self.pressionar(7))
		
		self.num_8 = Button(self.frame_numeros_top, text='8',
						command = lambda:self.pressionar(8))
		
		self.num_9 = Button(self.frame_numeros_top, text='9',
						command = lambda:self.pressionar(9))
		
		
		self.num_0 = Button(self.frame_numeros, text='0',
						command = lambda:self.pressionar(0))
		
		self.display.pack(side=TOP)

		self.num_7.pack(side=LEFT)
		self.num_8.pack(side=LEFT)
		self.num_9.pack(side=LEFT)
		self.num_0.pack()


		self.limpar = Button(self.frame_numeros, text='Limpar',
						command = self.limpar)

		self.branco = Button(self.frame_numeros, text='Banco',
						command = self.votar_branco)

		self.votar = Button(self.frame_numeros, text='Votar',
						command = self.buscar_dados_candidato)

		self.nulo = Button(self.frame_numeros, text='Nulo',
						command = self.votar_nulo)

		self.fim = Button(self.frame_numeros, text='Fim',
						command = self.finalizar)

		self.limpar.pack()
		self.branco.pack()
		self.votar.pack()
		self.nulo.pack()
		self.fim.pack()
		

		# self.frame_dados = Frame(self.)
		self.label_nome = Label(self.frame_numeros, text='Nome: ', fg='black', font=('Verdana', 10), height= 1)
		self.label_nome.pack(side=LEFT)
		self.display_nome = Text(self.frame_numeros, bd=2, height='1', width='10', font='Arial')
		self.display_nome.config(state = DISABLED)
		self.display_nome.pack(side=LEFT)
	
		self.label_numero = Label(self.frame_numeros, text='Numero: ', fg='black', font=('Verdana', 10), height= 1)
		self.label_numero.pack(side=LEFT)
		self.display_numero = Text(self.frame_numeros, bd=2, height='1', width='10', font='Arial')
		self.display_numero.config(state = DISABLED)
		self.display_numero.pack(side=LEFT)


	#funcoes para concatenar numeros no display	
	def pressionar(self, numero):
		texto = self.display.get("0.0", END)
		if len(texto) <= 3:
			self.display.insert(END, str(numero))

	def limpar(self):
		self.display.delete("0.0", END)

	def votar_branco(self):
		resp = tkMessageBox.askquestion("Voto Branco", "Deseja confirmar seu voto em branco?")
		if resp == 'yes':
			self.cliente.votar_branco()


	def buscar_dados_candidato(self):
		numero = int(self.display.get("0.0", END))
		
		# if numero in [111,222,333]:
		cand = cast(self.cliente.buscar_dados(numero), POINTER(Candidato))
		self.display_nome.config(state = NORMAL)
		self.display_numero.config(state = NORMAL)
		nome = cand.contents.nome
		self.display_nome.insert(END, nome)
		num = cand.contents.numero
		self.display_numero.insert(END, num)
		self.display_nome.config(state = DISABLED)
		self.display_numero.config(state = DISABLED)

		if nome != 'NULO':
			os.system("eog "+nome+".jpg")
		
		resp = tkMessageBox.askquestion("Confirmar Voto", "Deseja votar no "+nome+"?")
		if resp == 'yes':
			self.cliente.votar(num)
		
		self.display_nome.config(state = NORMAL)
		self.display_numero.config(state = NORMAL)
		self.display.delete("0.0", END)
		self.display_nome.delete("0.0", END)
		self.display_numero.delete("0.0", END)
		self.display_nome.config(state = DISABLED)
		self.display_numero.config(state = DISABLED)

	def votar_nulo(self):
		resp = tkMessageBox.askquestion("Voto Nulo", "Deseja confirmar seu voto em nulo?")
		if resp == 'yes':
			self.cliente.votar_nulo()

	def finalizar(self):
		cand = cast(self.cliente.verificar_vencedor(), POINTER(Candidato))
		urnas_restantes = self.cliente.finalizar_comunicacao()
		print 'Uras = ',urnas_restantes
		if urnas_restantes > 0:
			resp = tkMessageBox.showinfo("Resultado Parcial", "Candidato Na Liderança: "+cand.contents.nome)
		else:
			resp = tkMessageBox.showinfo("Resultado Final", "Candidato Vencedor: "+cand.contents.nome)
		app.destroy()

app = Tk()
Urna_Gui(app)

app.mainloop()