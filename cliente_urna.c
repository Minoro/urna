#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "urna.h"

#include "urna_clnt.c"
#include "urna_xdr.c"

CLIENT *clnt;


void estabelecer_comunicacao() {

    if(*(estabelecer_comunicacao_1((void *) NULL, clnt)) <= 0) {
        printf("Falha ao estabelecer comunicacao com o servidor.\n");
        exit(0);
    }
}

void conectar(char nome_servidor[100]) {
    /* cria uma struct CLIENT que referencia uma interface RPC */
    clnt = clnt_create (nome_servidor, URNA_PROG, URNA_VERSION, "udp");

    /* verifica se a referência foi criada */
    if (clnt == (CLIENT *) NULL) {
        clnt_pcreateerror (nome_servidor);
        exit(1);
    }

    estabelecer_comunicacao();
}

//decrementa no servidor o numero de urnas e retorna o numero atualizado de urnas existentes
int finalizar_comunicacao() {
    return *(finalizar_comunicacao_1((void *) NULL, clnt));
}

int votar(int num){
	if(*(votar_1(&num, clnt)) != 1)
        return 0;
    else
        return 1;
}

void * buscar_dados(int numero) {
    candidato *cand = buscar_dados_1(&numero, clnt);
    if(cand == NULL) {
        printf("Problemas no sistema.\n");
        exit(0);
    }
	
    return (void *)cand;
    // return *(cand);
}

void * verificar_vencedor() {
    candidato *cand = (verificar_vencedor_1((void *) NULL, clnt));
    return (void *) cand;
}

int votar_branco() {
    if(*(votar_branco_1((void *) 1, clnt)) <= 0)
        return 0;
    else
        return 1;
}

// int main(int argc, char const *argv[]) {
// 	CLIENT *clnt;
//     candidato cand_retorno;
	
//    /* verifica se o cliente foi chamado corretamente */
// 	if(argc!=2) {
// 		fprintf (stderr,"Usage: %s hostname\n",argv[0]);
// 		exit(1);
// 	}

//    /* cria uma struct CLIENT que referencia uma interface RPC */
// 	clnt = clnt_create (argv[1], URNA_PROG, URNA_VERSION, "udp");

//    /* verifica se a referência foi criada */
// 	if (clnt == (CLIENT *) NULL) {
// 		clnt_pcreateerror (argv[1]);
// 		exit(1);
// 	}
//     char opc;
//     char numero[20];
//     int num_cand;

//     estabelecer_comunicacao(clnt);
// 	while(1) {
// 		printf("\nEleicoes melhores do ano 2014.\nEntre com o numero do candidato: ");
// 		scanf("%s", numero);
//         if(strcmp(numero, "branco") == 0) {
//             if(votar_branco(clnt) == 1)
//                 printf("\a\a\aVoto em branco computado com sucesso.\n");
//             else
//                 printf("Falha ao computar voto.\n");
//             continue;
//         }
//         else if(strcmp(numero, "fim") == 0) {
//             if(finalizar_comunicacao(clnt) > 0) 
//                 printf("Resultado parcial das eleicoes. Vencedor:\n");
//             else
//                 printf("Resultado final das eleicoes:\n");            
//             cand_retorno = verificar_vencedor(clnt);
//             printf("Numero: %d\n", cand_retorno.numero);
//             printf("Nome: %s\n", cand_retorno.nome);
//             printf("Votos obtidos: %d\n", cand_retorno.votos);
//             return 0;
//         }
//         else
//             num_cand = atoi(numero);        
//         cand_retorno = buscar_dados(num_cand, clnt);
//         if(strcmp(cand_retorno.nome, "\0") == 0) {
//             printf("Candidato inexistente. Confirma o voto nulo? (s ou n) ");
//             fflush(stdin);
//             scanf("%c", &opc);
//             if(opc == 's') {
//                 votar(num_cand, clnt);
//                 printf("\a\a\aVoto computado com sucesso!\n");
//             }
//         }
//         else {
//             printf("Nome: %s\nNumero: %d\nConfirma voto? (s ou n) ", cand_retorno.nome, cand_retorno.numero);
//             fflush(stdin);
//             scanf(" %c", &opc);
//             if(opc == 's') {
//                 votar(num_cand, clnt);
//                 printf("\a\a\aVoto computado com sucesso!\n");
//             }
//             else
//                 printf("Voto cancelado pelo usuario.\n");
//         }
// 	}

// 	return 0;
// }
